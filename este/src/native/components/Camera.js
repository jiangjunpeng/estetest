/**
 * Created by jjp on 2017/5/23.
 */
import React, { Component } from 'react';
import {
  Platform,
  ScrollView,
  Navigator,
  View,
  StyleSheet,
  Text,

} from 'react-native';
import Camera from 'react-native-camera';
export default class CameraPage extends Component{

  render(){
    return(
      <Camera
        ref={(cam) => {
          this.camera = cam;
        }}
        style={styles.preview}
        aspect={Camera.constants.Aspect.fill}>
        <Text style={styles.capture} onPress={this._takePicture.bind(this)}>[CAPTURE]</Text>
      </Camera>
    );
  }
  _takePicture() {
    const options = {};
    //options.location = ...
    this.camera.capture({metadata: options})
      .then((data) => console.log(data))
      .catch(err => console.error(err));
  }

}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  },
});
