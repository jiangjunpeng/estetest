// @flow
import * as themes from '../themes';
import React, { Component } from 'react';
import {
  Platform,
  ScrollView,
  Navigator
} from 'react-native';
import {
  Box,
  Button,
  SwitchTheme,
  Text,
  ToggleBaseline,
} from '../../common/components';

import * as WeChat from 'react-native-wechat';
import CameraPage from '../components/Camera'

// // An example how to style existing component.
// const StyledSlider = (props) => (
//   <Box as={Slider} {...props} />
// );
// <StyledSlider
//   maximumValue={3}
//   height={2}
// />
export default class HomePage extends Component {

  render(){
    return (
    <Navigator
      initialRoute={{name:HomePageView,component:HomePageView}}

      configureScene={
        (router) =>{
          return Navigator.SceneConfigs.FloatFromRight;
        }
      }

      renderScene={(router,navigator)=>
        {
          let HomePage = router.component;
          return  <HomePage {...router.params} navigator={navigator}/>
        }
      }
    />
    );
  }

}
class HomePageView extends Component{

  constructor(props){
    super(props);
  }

  render(){
    return(
      <ScrollView>
        <Box paddingHorizontal={1} paddingTop={2}>
          <Text align="center" size={3}>
            Welcome to Este
          </Text>
          <Box marginVertical={1}>
            {Platform.OS === 'ios'
              ? <Box alignItems="center">
              <Text>Press Cmd+R to reload,</Text>
              <Text>Cmd+D or shake for dev menu.</Text>
            </Box>
              : <Box alignItems="center">
              <Text>Double tap R to reload,</Text>
              <Text>Cmd+M or shake for dev menu.</Text>
            </Box>}
          </Box>
          {/*<Button bold color="primary">*/}
          {/*Text Button*/}
          {/*</Button>*/}
          <Box marginBottom={1} >
            <Button primary onPress={this._Camera.bind(this)}>
              相机
            </Button>
            <Button  primary onPress={this._sendAuthRequest}>
              微信登录
            </Button>
          </Box>
          <SwitchTheme themes={themes} />
          <ToggleBaseline />
        </Box>
      </ScrollView>
    );
  }


  async _sendAuthRequest() {
    await WeChat.sendAuthRequest("snsapi_userinfo");
  }

  _Camera(){
    const {navigator} = this.props;
    if(navigator){
      navigator.push({
        name:'CameraPage',
        component:CameraPage,
      })
    }

  }
}

// export default HomePage;
